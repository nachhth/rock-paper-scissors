"use strict";

function getRandomInt(max) {
  return Math.floor(Math.random() * max + 1);
}

function getPCChoice() {
  let randomInt = getRandomInt(3);

  switch (randomInt) {
    case 1:
      return "paper";

    case 2:
      return "rock";

    case 3:
      return "scissor";
  }
}

function calculateWinner(userA, userB) {
  if (
    (userA === "scissor" && userB === "paper") ||
    (userA === "rock" && userB === "scissor") ||
    (userA === "paper" && userB === "rock")
  ) {
    return "ganaUserA";
  } else if (userA === userB) {
    return "tie";
  } else {
    return "ganaUserB";
  }
}

function isUserChoiceValid(userChoice) {
  switch (userChoice) {
    case "rock":
      return true;

    case "scissor":
      return true;

    case "paper":
      return true;

    default:
      return false;
  }
}

// console.log(isUserChoiceValid('rock'));

// console.log(isPCWinner('paper', 'rock'));

let userChoiceString;
let userGame;
let userScore = 0;
let pcScore = 0;

while (pcScore < 3 && userScore < 3) {
  userChoiceString = prompt("Choose: rock - scissor - paper");
  userGame = userChoiceString.toLowerCase();

  let pcGame = getPCChoice();

  switch (calculateWinner(pcGame, userGame)) {
    case "ganaUserA":
      pcScore = pcScore + 1;
      alert("¡¡ PC WINS !!");

      break;
    case "ganaUserB":
      userScore = userScore + 1;
      alert(`pcGame = ${pcGame} -- userGame = ${userGame} -- ¡¡ YOU WIN !!`);

      break;
    case "tie":
      alert(`pcGame = ${pcGame} -- userGame = ${userGame} -- ¡¡ it's a TIE !!`);

      break;
  }

  alert(`POINTS: pcWins = ${pcScore} -- userWins = ${userScore}`);
}

if (pcScore === 3) {
  alert("PC WINS THE GAME");
} else {
  alert("YOU WIN THE GAME");
}
